<?php
/*
 * Template Name: Form Entries
 * Description: Conditionally display the appropriate form entries to the user.
 */
 
get_header();

/* Start the Loop */
while ( have_posts() ) :
	the_post();
	?>

	<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

		
		<header class="alignwide">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		</header><!-- .entry-header -->
		

		<div class="entry-content">
			<div class="alignwide">
				<?php the_content(); ?>

				<h2>My Self-Assessment</h2>
				<?php echo FrmProDisplaysController::get_shortcode( array( 'id' => 29 ) ); ?>
				
				<?php 
					$user = wp_get_current_user();
					$allowed_roles = array( 'administrator', 'manager' );
					if ( array_intersect( $allowed_roles, $user->roles ) ) : ?>
					<h2>My Employee Assessments</h2>
					<?php echo FrmProDisplaysController::get_shortcode( array( 'id' => 25 ) ); ?>
				<?php endif; ?>
			</div>
		</div><!-- .entry-content -->

		<?php if ( get_edit_post_link() ) : ?>
			<footer class="entry-footer default-max-width">
				<?php
				edit_post_link(
					sprintf(
						/* translators: %s: Name of current post. Only visible to screen readers. */
						esc_html__( 'Edit %s', 'twentytwentyone' ),
						'<span class="screen-reader-text">' . get_the_title() . '</span>'
					),
					'<span class="edit-link">',
					'</span>'
				);
				?>
			</footer><!-- .entry-footer -->
		<?php endif; ?>
	</article><!-- #post-<?php the_ID(); ?> -->
<?php endwhile; // End of the loop.

get_footer();